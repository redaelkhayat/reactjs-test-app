# Reactjs test application
une application front-end créée à l'aide de Reactjs qui permet la gestion et l'affectation des ressources dans des divers projets.

### Structure du projet
```
index.html
src/
	components/
    	forms/
        	EditPerson.js
        	EditProject.js
        lists/
        	ListPersons.js
        	ListProjects.js
        App.js
        Loader.js
        Person.js
        Popup.js
        Project.js
	config.js
    Gruntfile.js
    index.js
    package.json
public/
fixtures/
	db.json
```
### Specs
* l'utilisateur peut consulter les listes des projets et les ressources;
* ajouter, modifier et supprimer un projet ou une ressource;
* consulter en detail un projet ou une ressource.

### Gruntfile.js
Votre fichier Gruntfile.js est déjà configuré vous n'avez qu'a
```
npm install -g grunt cli
cd /monprojet
npm install
```

Bonne application.