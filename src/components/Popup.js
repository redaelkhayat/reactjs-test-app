import React from "react";
const Popup = ({id, title, children}) => {
	return (
		<div id={`modal-${id}`} className="modal fade" tabIndex="-1" role="dialog">
		  <div className="modal-dialog" role="document">
		    <div className="modal-content">
		      <div className="modal-header">
		        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
		        	<span aria-hidden="true">&times;</span>
		        </button>
		        <h4 className="modal-title">{title}</h4>
		      </div>
		      <div className="modal-body">
		        {children}
		      </div>
		    </div>
		  </div>
		</div>
	);
};

export default Popup;