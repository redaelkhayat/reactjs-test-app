module.exports = function(grunt){

	var defaults = [
		"browserify",
		"uglify"
	];

	grunt.initConfig({

    browserify: {
      dist: {
        options: {
          transform: [
            ["babelify", {
              presets: ['es2015', 'stage-2', 'react']
            }]
          ]
        },
        files: {
          "../public/js/app.js": ["index.js"]
        }
      }
    },

		uglify: {
			dist: {
				files: {
					"../public/js/app.min.js": ["../public/js/app.js"]
				}
			}
		},

    watch: {
      scripts: {
        files: [
          './*.js',
          './components/**/*'
        ],
        tasks: defaults,
        options: {
          spawn: false,
        },
      }
    }

	});

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('w', ['watch']);
  grunt.registerTask('default', defaults);

};
